// This is the name of our package
// Everything with this package name can see everything
// else inside the same package, regardless of the file they are in
package main

// These are the libraries we are going to use
// Both "fmt" and "net" are part of the Go standard library
import (
	// "fmt" has methods for formatted I/O operations (like printing to the console)
	"fmt"
	// The "net/http" library has methods to implement HTTP clients and servers
	"net/http"
	// GUID generation library
	"github.com/segmentio/ksuid"
	// io library for handling file uploads
	"io/ioutil"
	// Importing filepath to get extension of upload
	"path/filepath"
	// using oslib for moving file
	"os"
)

// function to generate a unique identifier
// func getuuid() {
//	value := ksuid.New()
//	return value
//}

//func getext(input string) {
//	return filepath.Ext(input)
//}

func uploadfile(w http.ResponseWriter, r *http.Request) {
	fmt.Println("File Upload Endpoint Hit")
	var uuid = ksuid.New().String()
	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Oh, sweet Jesus You fucked it up! You absolute fucking retard!")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	uploadedfilename := uuid + filepath.Ext(handler.Filename)
	tempFile, err := ioutil.TempFile("uploads", uploadedfilename)
	// Rename the tempfile once uploaded
	os.Rename(tempFile.Name(), "uploads/"+uploadedfilename)
	fmt.Println(tempFile)

	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)
	// Needed to render filllink as html?
	w.Header().Set("Content-Type", "text/html; charset=utf-8 Location: /")
	// create a link to the new file
	filelink := "<a href=/uploads/" + uploadedfilename + ">" + uploadedfilename + "</a>"
	// return that we have successfully uploaded our file!
	fmt.Fprintf(w, "Here's your fucking file\n"+filelink)
}

func main() {
	// The "HandleFunc" method accepts a path and a function as arguments
	// (Yes, we can pass functions as arguments, and even treat them like variables in Go)
	// However, the handler function has to have the appropriate signature (as described by the "handler" function below)
	http.Handle("/", http.FileServer(http.Dir("./")))
	http.HandleFunc("/api/upload", uploadfile)

	// After defining our server, we finally "listen and serve" on port 8080
	// The second argument is the handler, which we will come to later on, but for now it is left as nil,
	// and the handler defined above (in "HandleFunc") is used
	http.ListenAndServe(":8080", nil)
}

// "handler" is our handler function. It has to follow the function signature of a ResponseWriter and Request type
// as the arguments.
func handler(w http.ResponseWriter, r *http.Request) {
	// For this case, we will always pipe "Hello World" into the response writer
	fmt.Fprintf(w, "Hello World!")
}
